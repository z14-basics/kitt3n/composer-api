<?php

namespace Kitt3n\ComposerApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Kitt3n\ComposerApi\Helper\Helper;
use Rmtram\JsonpParser;

/***
 *
 * This file is part of the "kitt3n | Composer API" composer package.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/
class Get
{
    const aErrorType = [
        'none' => 57498322,
        'api' => 10950455,
        'other' => 25467788,
    ];

    const aApiFormat = [
        'json',
        'jsonp',
    ];

    /**
     * 
     * @param string $sUrl
     * @param string $sFormat
     * @param array $aRequestOptions
     * @param array $aClientConfig
     * @param array $aReplaceInErrorMessage
     * @return array
     *
     * @example $aRequestOptions = [
     *   'query' => [
     *     'a' => 'b',
     *     'b' => 'c',
     *   ]
     * ]
     *
     * @example $aRequestOptions = [
     *   'Authorization' => [
     *     'Basic <cresdentials>,
     *   ]
     * ]
     *
     * @example $aRequestOptions = [
     *   'headers' => [
     *     'Authorization' => [
     *       'Basic <cresdentials>,
     *     ]
     *   ]
     * ]
     *
     * @example $aRequestOptions = [
     *   'auth' => [
     *     '<username>',
     *     '<password>',
     *   ]
     * ]
     */
    public static function getResponse(
        string $sUrl,
        string $sFormat = 'json',
        array $aRequestOptions = [],
        array $aClientConfig = [
            'verify' => true
        ],
        array $aReplaceInErrorMessage = [
            'search' => [],
            'replace' => [],
        ]
    ): array {

        /* @var array $aJsonResponse */
        $aJsonResponse = [
            'status' => 200,
            'time' => Helper::getExecutionTime(),
            'error' => [
                'type' => self::aErrorType['none'],
                'message' => '',
                'code' => '',
                'internal' => ''
            ],
            'response' => [],
        ];

        switch (true) {
            case !in_array($sFormat, self::aApiFormat):

                $aJsonResponse['error']['type'] = self::aErrorType['other'];
                $aJsonResponse['error']['message'] = 'Ooops! [' . __LINE__ . ']';
                $aJsonResponse['error']['internal'] = 93420350;

                break;
            default:

                /* @var null|\GuzzleHttp\Psr7\Response $oResponse */
                $oResponse = null;

                /* $var \GuzzleHttp\Client $oClient */
                $oClient = new Client(
                    $aClientConfig
                );

                try {

                    $oResponse = $oClient->request(
                        'GET',
                        $sUrl,
                        $aRequestOptions
                    );

                } catch (RequestException $e) {

                    $sErrorMessage = $e->getMessage();
                    if (array_key_exists('search', $aReplaceInErrorMessage)
                        && array_key_exists('replace', $aReplaceInErrorMessage)
                        && ! empty($aReplaceInErrorMessage['search'])
                        && ! empty($aReplaceInErrorMessage['replace'])) {
                        $sErrorMessage = Helper::mb_str_replace(
                            $aReplaceInErrorMessage['search'],
                            $aReplaceInErrorMessage['replace'],
                            $e->getMessage()
                        );
                    }

                    $aJsonResponse['status'] = ($oResponse == null ? null : $oResponse->getStatusCode());
                    $aJsonResponse['error']['type'] = self::aErrorType['api'];
                    $aJsonResponse['error']['message'] = 'Ooops! [' . __LINE__ . ']<br><br>' . $e->getMessage();
                    $aJsonResponse['error']['code'] = $e->getCode();
                    $aJsonResponse['error']['internal'] = 84574938;

                }

                /* $var int $iResponseStatusCode */
                $iResponseStatusCode = $oResponse->getStatusCode();
                $aJsonResponse['status'] = $iResponseStatusCode;

                switch (true) {
                    case $iResponseStatusCode != 200:

                        $aJsonResponse['status'] = $iResponseStatusCode;
                        $aJsonResponse['error']['type'] = self::aErrorType['api'];
                        $aJsonResponse['error']['message'] = 'Ooops! [' . __LINE__ . ']';
                        $aJsonResponse['error']['internal'] = 91840350;

                        break;
                    default:

                        /* @var \GuzzleHttp\Psr7\Stream $oBody */
                        $oBody = $oResponse->getBody();

                        /* @var string $sBodyContents */
                        $sBodyContents = $oBody->getContents();

                        ##
                        ## Extract charset from header "Content-Type'
                        ##
                        /* @var string $sCharset */
                        $sCharset = 'utf-8';

                        /* @var array $aContentType */
                        $aContentType = $oResponse->getHeader('Content-Type');

                        /* @var array $aTmp */
                        $aTmp = explode(';', $aContentType[0]);
                        if (!empty($aTmp)) {

                            foreach ($aTmp as $sItem) {

                                /* @var string $sItem */
                                if (strpos($sItem, 'charset') !== false) {

                                    /* @var array $aTmp_ */
                                    $aTmp_ = explode('=', $sItem);

                                    if (!empty($aTmp_)) {

                                        $sCharset = trim(strtolower($aTmp_[1]));

                                    }
                                }
                            }
                        }

                        ##
                        ## Change encoding to utf-8 if needed
                        ##
                        if ($sCharset != 'utf-8') {

                            /* @var string $sBodyContents */
                            $sBodyContents = iconv(
                                $sCharset,
                                'utf-8',
                                $sBodyContents
                            );

                        }

                        switch (true) {
                            case $sFormat == self::aApiFormat[0]:

                                /* @var array|null $vParsedBodyContents */
                                $vParsedBodyContents = json_decode(
                                    $sBodyContents,
                                    true
                                );

                                break;
                            case $sFormat == self::aApiFormat[1]:

                                /* @var \Rmtram\JsonpParser\Jsonp $oParser */
                                $oParser = new JsonpParser\Jsonp();

                                /* @var array|null $parsedBodyContents */
                                $vParsedBodyContents = $oParser
                                    ->decoder($sBodyContents)
                                    ->depth(1024)
                                    ->toArray();

                                break;
                            default:

                                /* @var array|null $parsedBodyContents */
                                $vParsedBodyContents = null;
                                $aJsonResponse['error']['type'] = self::aErrorType['other'];
                                $aJsonResponse['error']['message'] = 'Ooops! [' . __LINE__ . ']';
                                $aJsonResponse['error']['internal'] = 94040010;

                        }

                        switch (true) {
                            case $vParsedBodyContents == null:

                                $aJsonResponse['error']['type'] = self::aErrorType['other'];
                                $aJsonResponse['error']['message'] = 'Ooops! [' . __LINE__ . ']';
                                $aJsonResponse['error']['internal'] = 97049950;

                                break;
                            default:

                                $aJsonResponse['response'] = $vParsedBodyContents;

                        }
                }

                $aJsonResponse['time'] = Helper::getExecutionTime();

        }

        return $aJsonResponse;
    }
}